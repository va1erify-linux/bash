#!/bin/bash

if command -v docker &> /dev/null; then
  echo "Docker уже установлен"
  exit 0
else
  echo "Docker не найден. Запускаем установку..."
fi


echo "Удаление конфликтующих пакетов"
echo "for pkg in docker.io docker-doc docker-compose docker-compose-v2 podman-docker containerd runc; do sudo apt-get remove $pkg; done"
for pkg in docker.io docker-doc docker-compose docker-compose-v2 podman-docker containerd runc; do sudo apt-get remove $pkg; done

echo "Обновляем репозитории"
echo "sudo apt-get update"
sudo apt-get update

echo "Устанавливаем сертификаты и curl"
echo "sudo apt-get install ca-certificates curl"
sudo apt-get install -y ca-certificates curl

echo "sudo install -m 0755 -d /etc/apt/keyrings"
sudo install -m 0755 -d /etc/apt/keyrings

echo "sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg -o /etc/apt/keyrings/docker.asc"
sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg -o /etc/apt/keyrings/docker.asc

echo "sudo chmod a+r /etc/apt/keyrings/docker.asc"
sudo chmod a+r /etc/apt/keyrings/docker.asc


# Add the repository to Apt sources:
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/ubuntu \
  $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

echo "Обновление пакетов"
echo "apt-get update"
sudo apt-get update

echo "Устанавливаем docker&docker-compose"
echo "sudo apt-get install -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin"
sudo apt-get install -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

echo "Docker успешно установлен"
exit 0